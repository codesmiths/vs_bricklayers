﻿using Vintagestory.API.Common;
using Vintagestory.API.Datastructures;

namespace Bricklayers
{
    public class Bricklayers : ModSystem
    {
        public override void Start(ICoreAPI api)
        {
            base.Start(api);
            api.RegisterBlockEntityClass("BLPlanterCeramic", typeof(BECeramicContainer));
        }
    }
}
