#!/usr/bin/perl -w

use warnings;
use strict;

# Optimize a JSON file by:
# * removing trailing whitespace
# * converting \r\n to \n
# * Removing texture names from faces that are disabled
#   "south": { "texture": "#foo", "uv": [ 0.0, 0.0, 6.0, 2.5 ], "enabled": false },
#   =>
#   "south": { "texture": "#0", "uv": [ 0.0, 0.0, 6.0, 2.5 ], "enabled": false },
#
# Usage: perl build/optimize_json.pl FILE1.json [FILE2.json ... ]

sub fix_shape($);

for my $file (@ARGV)
  {
  warn("Cannot read $file: $!"), next unless -f $file;

  out("Reading: $file");
  open my $FH, '<', $file or die("Cannot read $file: $!");

  my $modified = 0;
  my $doc;
  # read input line by line
  while (my $line = <$FH>)
    {
    my $org_line = $line;
    $line =~ s/\r\n/\n/;		# convert \r\n to \n
    $line =~ s/\s+\n/\n/;		# remove trailing whitespace

    # turn '{  }' into '{ }'
    $line =~ s/\{  \}/{ }/g;

    $line = fix_shape($line);		# do we need to optimize something in a shape file?

    # remember all the content
    $doc .= $line;

    # did we modify the file in any way?
    $modified = 1 if $line ne $org_line;
    }
  close $FH;

  if ($modified)
    {
    # make backup?
    out("Contents modified, writing back: $file");
    rename $file, "$file.bak";
    open my $FH, '>', $file or die("Cannot write back to $file: $!");
    print $FH $doc;
    close $FH;
    }
  }

sub out
  {
  my $msg = join("", @_);

  print $msg,"\n";
  }

sub fix_shape($)
  {
  my ($line) = @_;

  # "up": { "texture": "#leaves5", "uv": [ 0.0, 0.0, 16.0, 0.5 ], "enabled": false, "windData": [1,1,1,1] },
  # "down": { "texture": "#leaves5", "uv": [ 0.0, 0.0, 6.0, 6.0 ], "enabled": false, "windMode": [-1,-1,-1,-1] }

  # remove windData on disabled faces
  $line =~ s/"enabled": false, "wind(Mode|Data)": \[[0-9,\.-]+\]/"enabled": false/
	if $line =~ /(south|north|west|east|up|down)/;
  # and also again for windMode
  $line =~ s/"enabled": false, "wind(Mode|Data)": \[[0-9,\.-]+\]/"enabled": false/
	if $line =~ /(south|north|west|east|up|down)/;

  # fix
  # "south": { "texture": "#foo", "uv": [ 0.0, 0.0, 6.0, 2.5 ], "enabled": false },
  # to be
  # "south": { "texture": "#0", "uv": [ 0.0, 0.0, 6.0, 2.5 ], "enabled": false },
  $line =~ s/(\{ "texture": "#)[a-zA-Z0-9_-]+(", "uv":.*, "enabled": false)/${1}0$2/
	if $line =~ /(south|north|west|east|up|down)/;

  # return the potentially modified line
  $line;
  }
