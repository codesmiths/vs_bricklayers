#!/usr/bin/perl -w

# Generates a list of names/translations for glazed items

# Usage: perl build/generate_names.pl en glazedplanter "Glazed planter"
# Usage: perl build/generate_names.pl en glazedflowerpot "Glazed flowerpot"
# Usage: perl build/generate_names.pl en glazedbowl "Glazed bowl" --nopattern
# Usage: perl build/generate_names.pl de

use strict;
use warnings;
$|++;	# output buffering off

# commandline options
my $lang = shift // 'en';
my $item = shift // 'glazedplanter';
my $name = shift // 'Glazed planter';

my $no_pattern = shift // 0;

my @states = qw/raw burnt/;
my $s_states = {
	en => { raw => "Raw", burnt => "" },
	de => { raw => "Roher", burnt => "" },
	}
	;

my @types = qw/clear milky/;
my $s_types = {
	en => { clear => "", milky => "Milky " },
	de => { clear => "", milky => "Matt-" }
	};

my @colors = ( "black", "blue", "brown", "gold", "green", "greenblue", "lapislazuli",
	       "malachite", "orange", "pink", "purple", "red", "redbrown", "white", "yellow" );
my $s_colors = {
	en => { 
	       	black => 'Black', blue => 'Blue', brown => 'Brown', gold => 'Gold',
		green => 'Green', greenblue => 'Greenblue', lapislazuli => 'Lapis lazuli',
		malachite => 'Malachite', orange => 'Orange',
	       	pink => 'Pink', purple => 'Purple', red => 'Red',
		redbrown => 'Redbrown', white => 'White', yellow => 'Yellow' 
	},
	de => {
	       	black => 'Schwarz', blue => 'Blau', brown => 'Braun', gold => 'Gold',
		green => 'Grün', greenblue => 'Grünblau', lapislazuli => 'Lapislazuli',
		malachite => 'Malachit', orange => 'Orange',
	       	pink => 'Rosa', purple => 'Purpur', red => 'Rot',
		redbrown => 'Rotbraun', white => 'Weiß', yellow => 'Gelb' }
};

my @patterns = qw/plain checker lines/;
my $s_patterns = {
       en => { plain => "", checker => "Checker", lines => "Lines" },
       de => { plain => "", checker => "Kariert", lines => "Linien" },
};

my $template = "\"bricklayers:block-##ITEM##-##STATE##-##TYPE##-##COLOR##-##PATTERN##\":\t\"##S_STATE####NAME## (##S_TYPE####S_COLOR####S_PATTERN_SEP####S_PATTERN##)\"";

if ($no_pattern)
  {
  @patterns = qw/empty honey milk/;
  $s_patterns = {
	en => { empty => "", milk => "Milk", honey => "Honey" },
	de => { empty => "", milk => "Milch", honey => "Honig" }
  };
  }

my $pattern_sep = { en => ' ', de => ', ' };

my $max = 0;			# max. length of first part
my @lines = ();
for my $state (@states)
  {
  my $s_state = $s_states->{$lang}->{$state};
  my $s_name = $name;
  if ($s_state)
    {
    $s_state .= ' ';
    # "Raw glazed" vs. "Glazed"
    $s_name = lc(substr($name,0,1)) . substr($name,1, length($name) - 1);
    }
  for my $type (@types)
    {
    my $s_type = $s_types->{$lang}->{$type};
    for my $pattern (@patterns)
      {
      my $s_pattern = $s_patterns->{$lang}->{$pattern};
      my $psep = $s_pattern ? $pattern_sep->{$lang} : '';

      push @lines, [];
      for my $color (@colors)
        {
	my $s_color = $s_colors->{$lang}->{$color} // $color;

	my $tpl = $template;
	$tpl =~ s/##ITEM##/$item/;
	$tpl =~ s/##STATE##/$state/;
	$tpl =~ s/##TYPE##/$type/;
	$tpl =~ s/##COLOR##/$color/;
	$tpl =~ s/##PATTERN##/$pattern/;
	$tpl =~ s/##S_STATE##/$s_state/;
	$tpl =~ s/##NAME##/$s_name/;
	$tpl =~ s/##S_TYPE##/$s_type/;
	$tpl =~ s/##S_COLOR##/$s_color/;
	$tpl =~ s/##S_PATTERN##/$s_pattern/;
	$tpl =~ s/##S_PATTERN_SEP##/$psep/;

	# left-align right part: if the color is <= 8 chars, add another "\t"
	my $first_part = $tpl; $first_part =~ /^\t/; $first_part =~ s/":.*/"/;
	my $second_part = $tpl; $second_part =~ s/.*:\t//;
	$max = length($first_part) if length($first_part) > $max;
	push @lines, [ $first_part, $second_part ];
	}
      }
    }
  }

my $right = int((2 + $max + 8) / 8) * 8;	# where the right side must begin with tab=8 

# print the result
while (my $line = shift @lines)
  {
  my ($first, $second) = @$line;

  if (!$first)
   {
   print "\n";
   next;
   }
  my $comma = @lines > 0 ? ',' : '';

  my $padd = '';
  while ((length($first . ":" . $padd) % 8) != 0)
    {
    $padd .= ' ';
    }
  my $tabs = 0;
  while (length($first . ":" . $padd . (" " x (8 * $tabs))) < $right)
    {
    $tabs ++;
    }
  my $l = length($padd . (" " x (8 * $tabs)));

  $tabs ++ if length($padd) > 0;
  $padd = "\t" x $tabs;

  print "\t$first:$padd$second$comma\n";
  }

# done

