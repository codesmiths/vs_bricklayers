#!/usr/bin/perl -w

# read int the complete input from STDIN
my $tpl;
{
  local $/ = undef;
  $tpl .= <>;
}

print "[\n";
for my $color (qw/
	black blue brown gold green greenblue lapislazuli
	malachite orange pink purple red redbrown white yellow
	/
  )
  {
  my $doc = $tpl;
  $doc =~ s/\{color\}/$color/mg;
  $doc =~ s/\n\z/,\n\n/ unless $color eq 'yellow';
  print $doc;
  }
print "]\n";

