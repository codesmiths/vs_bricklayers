#!/bin/sh

if [ $(cat modinfo.json | jq -r .type) = 'code' ]
then
  echo "Code build no longer supported with .NET 7"
  exit 1
else
  echo "Mod is content only, skipping code build."
  exit 0
fi
