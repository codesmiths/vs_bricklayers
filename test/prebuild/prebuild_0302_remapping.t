#!/usr/bin/perl

# Test that entries for the remapper point to existing items or blocks.

use Test::More;
use warnings;
use strict;
use utf8;

# load the test helper library
use lib 'test/lib';
use VSJSON;

# subroutine definitions
sub test_stock($ $ $ $);

# The global number of tests we did run
my $number_of_tests_run = 0;

use constant TEST_PATH => 'assets/';
use constant DEBUG => 0;

# Step 1: Gather all JSON info
# Step 2: Test JSON patches

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

my %seen_entries;		# to check for duplicates

# Test patches to be valid
for my $json_file (sort @{ $info->{patches} } )
  {
  # only test the remapping patch
  next unless $json_file =~ /game-config-remaps/;

  my $json = VSJSON::parse_json($json_file);
  is (ref($json), "ARRAY", "$json_file has valid JSON ARRAY");
  $number_of_tests_run ++;

  next unless ref($json) eq 'ARRAY';

  my $idx = 0;
  for my $patch (@{ $json })
    {
    is (ref($patch), 'HASH', 'Patch $idx is an object');
    $number_of_tests_run ++;

    is (ref($patch->{value}), 'ARRAY', 'Patch $idx: value is an ARRAY');
    $number_of_tests_run ++;

    $idx ++;
    }

  my $patch_index = 0;
  # for all patches in this file
  for my $patch (@$json)
    {
    $patch_index ++;

    is (ref($patch), 'HASH', 'Patch $idx is an object');
    $number_of_tests_run ++;

    is (ref($patch->{value}), 'ARRAY', 'Patch $idx: value is an ARRAY');
    $number_of_tests_run ++;

    # we do not need to test more basic patch properties, as these will be
    # tested along with all other patches. So test only remapping specific stuff:

    # Example:
    # [
    #   {
    #   "op": "add",
    #   "path": "/game:v1.18expaned-matter-2",
    #   "value": [
    #     "/iir remap em:crushed-metal-temporal em:crushedluminous-metal-temporal force"
    #   ],
    #   "file": "game:config/remaps.json"
    #   }
    # ]

    my $value = $patch->{value};

    my $v = 1;
    for my $val (@$value)
      {
      like ($val, qr/^\/[bi]ir remap [^ ]+ [^ ]+/, "Entry $v '$val' is well formatted.");
      $number_of_tests_run ++;

      unlike ($val, qr/::/, "Entry $v '$val' is well formatted.");
      $number_of_tests_run ++;
      $v ++;

      $val =~ qr/^\/([bi])ir remap ([^ ]+) ([^ ]+)/;

      my $type = $1 eq 'b' ? 'block' : 'item';
      my $code = $2;
      my $source = $3;
      my $src_modid = $source; $src_modid =~ s/:.*//;

      if (exists $seen_entries{$code})
        {
	# allow the same entry from a different modid
	my $my_modid = $code; $my_modid =~ s/:.*//;
	for my $other (@{$seen_entries{$code}})
	  {
	  if ($other eq $my_modid)
	    {
	    isnt( $other, $my_modid, "Saw entry $code in '$val' before.");
	    $number_of_tests_run ++;
	    }
	  }
	}
      $seen_entries{$code} = [] unless ref $seen_entries{$code};
      push @{$seen_entries{$code}}, $src_modid;

      if ($code =~ /^$modid:/)
        {
        my $rc = VSJSON::check_code($info, $json_file, $code, $code, $type);

	is ($rc, undef, "$json_file:$code is valid");
        $number_of_tests_run ++;
	}
      }
    }
  }

done_testing( $number_of_tests_run );

# Subroutines

