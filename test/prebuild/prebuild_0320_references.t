#!/usr/bin/perl

# Test that JSON items or blocks references for textures, shapes, or other items/blocks
# point to existing objects.

use Test::More;
use warnings;
use strict;
use utf8;

# load the test helper library
use lib 'test/lib';
use VSJSON;

my $VERSION = '0.0.3';

# subroutine definitions
sub test_texture($ $ $ $ $ $ $ $ $);
sub check_code($ $ $ $ $ $);
sub test_shape($ $ $ $ $ $);
sub test_liquid_properties($ $ $ $ $);

# The global number of tests we did run
my $number_of_tests_run = 0;

# test each file only once to speed up the tests
my $files_tested = {};

use constant TEST_PATH => 'assets/';
use constant DEBUG => 0;

# Step 1: Gather all JSON info
# Step 2: Test JSON references

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

# Test JSON references to be valid
for my $json_file (sort @{ $info->{itemtypes} }, @{ $info->{blocktypes} } )
  {
  my $json = VSJSON::parse_json($json_file);

  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  # we expect only one item/block per JSON file
  is (ref($json), 'HASH', 'Expected an object, not an array');
  $number_of_tests_run ++;

  print "# $json_file\n" if DEBUG > 0;

  # build a list of valid states, so we can check textures referencing them
  my $states = [];
  if (exists $json->{variantgroups})
    {
    for my $state (@{$json->{variantgroups}})
      {
      my ($scode, $sstates) = VSJSON::generate_states_from_json($json_file, $state);

      push @$states, [ $scode, $sstates ];
      }
    }

  # for all properties in the object
  for my $key (sort keys %$json)
    {
    if ($key eq 'texture')
      {
      test_texture( \$number_of_tests_run, $json_file, $info, $states, $key, $json->{$key}, 0, undef, {} );
      }
    elsif ($key eq 'textures')
      {
      for my $entry (sort keys %{ $json->{$key} })
        {
        test_texture( \$number_of_tests_run, $json_file, $info, $states, "$key $entry", $json->{$key}->{$entry}, 0, undef, {} );
	}
      }
    elsif ($key eq 'texturesByType')
      {
      my $done_variants = {};
      for my $entry (sort {
		# longest entries first
		length($b) <=> length($a) or
		$b cmp $a
		}
		keys %{ $json->{$key} })
        {
	# extract the type name from the key ("*-obsidian-*" => "obsidian")
	my $type = $entry; $type =~ s/[\*-]//g;

        for my $text (sort keys %{ $json->{$key}->{$entry} })
	  {
	  test_texture( \$number_of_tests_run, $json_file, $info, $states, "$key $entry $text", $json->{$key}->{$entry}->{$text}, 0, undef, $done_variants );
	  }
	}
      }
    elsif ($key =~ /^shape(inventory)?$/i)
      {
      test_shape( \$number_of_tests_run, $info, $json_file, $states, $key, $json->{$key} );
      }
    elsif ($key =~ /^shape(inventory)?ByType$/i)
      {
      for my $entry (sort {
		# longest entries first
		length($b) <=> length($a) or
		$b cmp $a
		}
		keys %{ $json->{$key} })
        {
	test_shape( \$number_of_tests_run, $info, $json_file, $states, "$key $entry", $json->{$key}->{$entry} );
	}
      }
    elsif ($key eq 'attributes')
      {
      my $att = $json->{$key};
      my $text = 'inContainerTexture';
      if (exists $att->{$text})
	{
	  test_texture( \$number_of_tests_run, $json_file, $info, $states, "$key attributes $text", $att->{$text}, 0, undef, {} );
	}
      test_liquid_properties( \$number_of_tests_run, $info, $json_file, $states, $att->{liquidContainerProps})
	if exists $att->{liquidContainerProps};
      }
    elsif ($key eq 'attributesByType')
      {
      for my $entry (sort {
		# longest entries first
		length($b) <=> length($a) or
		$b cmp $a
		}
		keys %{ $json->{$key} })
        {
	my $att = $json->{$key}->{$entry};
	next unless ref($att);
	if (exists $att->{mealBlockCode})
	  {
	  check_code( \$number_of_tests_run, $info, $json_file, "$key $entry mealBlockCode", $att->{mealBlockCode}, 'block');
	  }
	my $text = 'inContainerTexture';
	if (exists $att->{$text})
	  {
	  test_texture( \$number_of_tests_run, $json_file, $info, $states, "$key $entry $text", $att->{$text}, 0, undef, {} );
	  }
        test_liquid_properties( \$number_of_tests_run, $info, $json_file, $states, $att->{liquidContainerProps})
		if exists $att->{liquidContainerProps};
	}
      }
    elsif ($key eq 'combustiblePropsByType')
      {
      for my $entry (sort {
		# longest entries first
		length($b) <=> length($a) or
		$b cmp $a
		}
		keys %{ $json->{$key} })
        {
	my $att = $json->{$key}->{$entry};
	if (ref($att) && exists $att->{smeltedStack})
	  {
	  my $smelted = $att->{smeltedStack};
	  ok (ref($smelted), "$key -> smeltedStack is a reference");
	  my $type = $att->{smeltedStack}->{type} // 'unknown';
	  like ($type, qr/^(block|item)$/, "$key -> smeltedStack -> $type is a valid type");
	  $number_of_tests_run += 2;
	  check_code( \$number_of_tests_run, $info, $json_file, "$key $entry smeltedStack", $smelted->{code}, $type);
	  }
	}
      }
    elsif ($key eq 'transitionablePropsByType')
      {
      for my $entry (sort {
		# longest entries first
		length($b) <=> length($a) or
		$b cmp $a
		}
		keys %{ $json->{$key} })
        {
	my $att = $json->{$key}->{$entry};
	is (ref($att), 'ARRAY', "$key -> $entry is an ARRAY");
	$number_of_tests_run ++;
	if (ref($att) eq 'ARRAY')
	  {
	  for my $t (@$att)
	    {
	    like ($t->{type}, qr/^(Cure|Harden|Perish|Dry)$/, "$key -> $entry -> type -> $t->{type} is a valid transition type");
	    my $trans = $t->{transitionedStack};
	    ok (ref($trans), "$entry -> transitionedStack is a reference");
	    my $type = $trans->{type} // 'unknown';
	    like ($type, qr/^(block|item)$/, "$key -> $entry -> transitionedStack -> $type is a valid type");
	    $number_of_tests_run += 3;

	    check_code( \$number_of_tests_run, $info, $json_file, "$key $entry transitionedStack", $trans->{code}, $type);
	    }
	  }
	}
      }
    }
  }

done_testing( $number_of_tests_run );

#############################################################################
# Subroutines

sub test_texture($ $ $ $ $ $ $ $ $)
  {
  my ($tests, $file, $info, $states, $name, $value, $level, $onlystate, $done_variants) = @_;

  is (ref($value), 'HASH', "$file: $name is a reference");
  $$tests ++;

  # "texturesByType": {
  #  "*-raw-*": {
  #  		"ceramic": { "base": "game:block/clay/ceramic" },
  #             "top":	   { "base": "bricklayers:block/ceramic/glazed/{state}/{type}/{color}",
  #			     "overlays": [ "bricklayers:block/ceramic/glazed/pattern/{pattern}" ] },

  # we expect only "alternates", "base" or "overlays" or "rotation"
  # "alternates" should only present if we are at level 0
  my $found = 0;
  for my $key (sort keys %{$value})
    {
    $found ++ if $key =~ /^(base|overlays|blendedoverlays|rotation|alpha)/i;
    $found ++ if $level == 0 && $key eq 'alternates';
    }
  is (scalar keys %$value, $found, "$file: Texture reference $name has 'base' and only optional alternates or overlays");
  $$tests ++;
  # speed up testing: only check alpha if it is actually used
  if (defined $value->{alpha})
    {
    my $alpha = $value->{alpha};
    is (int($alpha), $alpha, "$file: Texture alpha is an integer");
    ok ($alpha >= 0 && $alpha <= 255, "$file: Texture alpha is between 0 and 255");
    $$tests += 2;
    }

  # if this texture has alternates, test these, too
  if (exists $value->{alternates})
    {
    is (ref($value->{alternates}), 'ARRAY', "$file: $name alternates is an array");
    $$tests ++;
    my $idx = 0;
    for my $alternate (@{ $value->{alternates} })
      {
      test_texture($tests, $file, $info, $states, "$name alternate $idx", $alternate, $level + 1, $onlystate, $done_variants);
      $idx++;
      }
    }

  if (exists $value->{rotation})
    {
    like($value->{rotation}, qr/^(0|90|180|270)/, "$file: Texture reference $name has valid rotation");
    $$tests ++;
    }

  my @todo_entries = ($value->{base});
  push @todo_entries, @{ $value->{overlays} } if ref($value->{overlays});

  ALTERNATIVE:
  for my $tfile (@todo_entries)
    {
    # Continue even for game: etc. entries so we mark the variant as "done"

    # If we have templates like "{pattern}" in the filename
    #
    # "texturesByType": {
    #  "*-a-*": { "top": { "base": "bricklayers:block/ceramic/glazed/{state}/{type}/" } }
    #
    # $name is here now "*-raw-*" and the following variants exists
    # "variantGroups": [
    #   "state": [ "a", "b" ],
    #   "type": [ "c", "d" ],
    # ]
    # and
    # "skipVariants": [ "item-a-d", "item-b-c" ]
    # this means in $info we have the following list of valid codes:
    #   item-a-c
    #   item-b-d
    # We need to match each of these against $name so we only check "item-a-c"

    # Even if we don't have patterns in the name, we need to mark all matching variants as done

    # skip further tests for legacy files with deprecated codes
    next ALTERNATIVE if $file =~ /legacy/;

    my $base_code = $info->{file_codes}->{$file};
    if (!$base_code)
      {
      $tests++;
      ok (defined $base_code, "Cannot map $file back to the item or block code");
      return;
      }

    # Build a list of variants valid only for this file
    # TODO: VSJSON.pm could remember which variant are valid for which file for us
    my %local_variants;
    for my $v (keys %{$info->{variants}})
      {
      # in case of "ash", use "ash-a", but not "ashparticle-b"
      $local_variants{$v} = $info->{variants}->{$v} if $v =~ /^$base_code\-/;
      }

    # from the list of states
    # [
    #    [ 'category',  [ 'organic' ] ],
    #    [ 'type',      [ 'plant', 'wood', 'sludge' ]
    # ]
    # and the list of valid variants, generate one filename for each valid variant

    my $matches = 0;
    # something like "texture"
    my $name_qr = $name;
    if ($name =~ /\s([^\s]+)/)
      {
      # attributes attributes inContainerTexture => '*' (valid for all variants)
      $name_qr = $1;
      $name_qr = '*' if $name_qr !~ /[\(\*]/;
      }
    # else is this a regexp?
    if ($name_qr =~ /^\@/)
      {
      # regexp
      $name_qr =~ s/^\@//;
      }
    else
      {
      # something like "*-red-*"
      $name_qr =~ s/\*/\.\*/g;
      # into ".*-red-.*"
      }

    print STDERR "Looking at name '$name' => '$name_qr'\n" if DEBUG;
    my @todo;
    for my $variant (keys %local_variants)
      {
      # print STDERR "Examining variant $variant ($name_qr)\n" if DEBUG;

      # print "Matching $variant vs $name_qr\n";
      # only use variants that match $name
      if ($variant =~ /$name_qr/)
        {
        $matches ++;
	next if exists $done_variants->{$variant};

        # variants that are matched by say "*-red-*" are not considered again for "*"
	# remember that we processed it
        $done_variants->{$variant} = undef;

        # skip the test if this file points to game: etc
        next unless $tfile =~ /^$modid:(.*)/;
	my $new_file = "assets/$modid/textures/$1.png";

	# turn "red*.png" into "red1.png" (we test only that at least the first variant exists)
	$new_file =~ s/\*.png$/1.png/;

	# if the file has not {name}-patterns in, just use it as it is
	push @todo, $new_file and next if $new_file !~ /\{.*?\}/;

	print STDERR "Done $variant ($name_qr) (" . (scalar keys %$done_variants) . " done)\n" if DEBUG;

        # from ash-organic-wood we need to produce "base = ash, category = organic, type = wood"
	my $idx = 1;	# ignore the first part
	# if we have something like "glazedbowl-meal" as base code, we need to remove two parts at the front
	my $removal = $base_code; $removal =~ s/-/ $idx++; /eg;

	my @parts = split /-/, $variant;
	# replace {...} in "somepath/name/{abc}/{xyz}/" with each corrosponding entry for this variant
	# "texture/ash/{type}/{category}" => "texture/ash/wood/organic" OR
	# "texture/ash/{category}/{type}" => "texture/ash/organic/wood"
	for my $state (@$states)
          {
	  my $part = $parts[$idx++];
	  # print STDERR "Producing $part ($idx) for state $state->[0] for $name_qr\n" if DEBUG;
	  $new_file =~ s/\{$state->[0]\}/$part/g;
	  }
	# we produced a new file name
	push @todo, $new_file;
	}
      }

    FILE:
    while (my $current = shift @todo)
      {
      # test each entry only once
      next FILE if exists $files_tested->{$current};

      # test this entry as plain filename
      my $size = (-s $current) // 0;
      my $isfile = -f _;
      if ($isfile)
        {
	ok ($size > 100, "File $current too small ($size byte < 100 byte) (referenced in $file).");
	$$tests ++;
	}
      ok ($isfile, "File $current: $! (referenced in $file)");
      $$tests ++;
      return if !$isfile || $size < 100;

      $files_tested->{$current} = undef;		# remember that we tested that file
      } # end for all variants of this entry

    } # end for this entry

  # all tests done
  }

sub check_code($ $ $ $ $ $)
  {
  # check that the given code is a valid one
  my ($tests, $info, $file, $name, $code, $type) = @_;

  my $rc = VSJSON::check_code($info, $file, $name, $code, $type);

  is ($rc, undef, "$file:$code is valid");
  $$tests ++;
  }


sub test_shape_name($ $ $ $ $ $)
  {
  my ($tests, $info, $file, $states, $name, $code) = @_;

  # also allow references to mods we depend on
  like ($code, $info->{valid_codes}, "$file: ${name}'s shape reference $code has a valid modid");
  $$tests ++;

  # cannot test game: references yet
  return unless $code =~ /^$modid:/;

  # cannot test references with "{somestate}" in it yet
  return if $code =~ /\{/;

  # "modid:foo/bar" => "assets/modid/shapes/foo/bar.json"
  $code =~ s/^$modid:/assets\/$modid\/shapes\//;
  $code .= '.json';

  # check that "assets/$modid/shapes/$code" exists
  is (-f $code, 1, "Shape $code exists");
  $$tests ++;
  }


sub test_shape($ $ $ $ $ $)
  {
  my ($tests, $info, $file, $states, $name, $value) = @_;

  is (ref($value), 'HASH', "$file: $name is a reference");
  $$tests ++;

  #	"shapeByType": {
  #		"*-north": { "base": "game:block/clay/crock/base", "rotateY": 0 },
  #	"shapeinventoryByType": {
  #		"*-north": { "base": "game:block/clay/crock/base", "rotateY": 0 },
  # or:
  # 	"shape": { "base": "game:item/liquid" }

  # we expect only "base", or rotateX, rotateY, rotateZ
  my $found = 0;
  my $code = 'notfound';
  for my $key (sort keys %{$value})
    {
    $found ++ if $key =~ /^(base|rotate[XYZxyz]|scale|overlays|offset[XYZxyz])$/;
    $code = $value->{$key} if $key eq 'base';
    }
  my $cnt = scalar keys %$value;
  is ($found, $cnt, "$file: Shape reference $name has 'base' and only optional rotation or scale");
  if ($cnt != $found)
    {
      require Data::Dumper;
      print STDERR Data::Dumper::Dumper($value);
    }

  $$tests ++;

  test_shape_name( $tests, $info, $file, $states, $name, $code );
  }


sub test_liquid_properties($ $ $ $ $)
  {
  my ($tests, $info, $file, $states, $att) = @_;

  is (ref($att), 'HASH', "$file: attributes->liquidContainerProps is a reference");
  $$tests ++;

  for my $shapeloc (qw/emptyShapeLoc opaqueContentShapeLoc liquidContentShapeLoc/)
    {
    is (ref($att->{$shapeloc}), '', "$file: attributes->liquidContainerProps->$shapeloc is not reference");
    $$tests ++;
    test_shape_name( $tests, $info, $file, $states, $shapeloc, $att->{$shapeloc} );
    }

  #  test liquid amounts
  for my $key (qw/capacityLitres transferSizeLitres liquidMaxYTranslate/)
    {
    ok ( $att->{$key} > 0, "$file: $key > 0");
    $$tests ++;
    }
  ok ( $att->{capacityLitres} >= $att->{transferSizeLitres}, "$file: capacityLitres >= transferSizeLitres");
    $$tests ++;
  }


# End of code

