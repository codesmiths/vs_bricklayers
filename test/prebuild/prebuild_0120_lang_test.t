#!/usr/bin/perl

# Test the language files:
#
# * that they do not contain double entries
# * that translated entries are present in en.json
# * that refered-to handbook titles and sections exist

use Test::More;
use JSON;
use File::Find;
use warnings;
use strict;
use utf8;
use Encode qw/decode/;

# load the test helper library
use lib 'test/lib';
use VSJSON;

our $VERSION = '0.0.2';

my $number_of_tests_run = 0;

use constant TEST_PATH => 'assets/';
use constant DEBUG => 0;

my $generate_missing = shift;		# should we generate files with missing translation entries?

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

my $dep = join('|', @{$info->{dependencies}});

my @LANGUAGES;

# define the subroutine to gather all JSON files
sub is_json {
  push @LANGUAGES, $File::Find::name
	if -f $File::Find::name && $File::Find::name =~ /\.json\z/;
};

# Gather all files
find({ wanted => \&is_json, no_chdir => 1 }, TEST_PATH . $modid . '/lang/');

use constant QR_VALID_LINE		=> qr/^[\s\t]*"([a-z]+[a-z0-9-]+):([^"]+)"[\s\t]*:[\s\t]*"(([^"]|\\")+)",?[\s\t]*\z/;
use constant QR_VALID_DOMAINLESS	=> qr/^[\s\t]*"()([^"]+)"[\s\t]*:[\s\t]*"(([^"]|\\")+)",?[\s\t]*\z/;
use constant QR_SINGLE_COLON		=> qr/^[\s\t]*"[^"]*:[^"]*:/;
use constant QR_ENGLISH			=> qr/\b(when|other|ground|powder|crushed|planter|flower.?pot|vessel|brick)\b/i;

my %english;

binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';
# Test each file, sorted by name
# - We sort them so that english comes first and we can record all English strings
# - Then for each other language, we can detect untranslated strings
FILE:
for my $file (sort {
	my $a_score = ($a =~ /\/en\.json/) ? 1000 : 1;
	my $b_score = ($b =~ /\/en\.json/) ? 1000 : 1;
        $b_score <=> $a_score || $a cmp $b
	} @LANGUAGES)
  {

  # read exceptions for translations that should override game translations
  my $exception_file = $file; $exception_file =~ s/assets\/$modid\/lang\//test\/prebuild\/lang_exceptions\//;
  my $exceptions = { entries => [] };

  $exceptions = VSJSON::parse_json( $exception_file ) if -f $exception_file;

  open (my $FH, '<', $file) or fail("$file - $!"), next;

  # parse the file, recording every entry
  my %entries;
  my $linenr = -1;
  my $saw_open = 0;
  my $saw_close = 0;

  LINE:
  while (my $rawline = <$FH>)
    {
    $linenr++;

    my $line = decode('UTF-8', $rawline, Encode::FB_CROAK);

    # skip empty lines
    next LINE if $line =~ /^[\s\t\r\n]+\z/;

    $line =~ s/[\r\n]+//;

    $saw_open ++ if $line =~ /^[\s\t]*[{][\s\t]*\z/;
    $saw_close ++ if $line =~ /^[\s\t]*[}][\s\t]*\z/;

    # skip "{" or "}"
    next LINE if $line =~ /^[\s\t]*[{}][\s\t]*\z/;

    my $exception;
    if ($line =~ QR_SINGLE_COLON)
      {
      unlike($line, QR_SINGLE_COLON, "$file line $linenr: Invalid double ':'");
      $number_of_tests_run ++;
      next FILE;
      }
    my ($domain, $entry, $translation);
    # my ($domain, $entry, $translation);
    # "modid:item-something":               "Some text"
    if ($line !~ QR_VALID_LINE)
      {
      if ($line =~ QR_VALID_DOMAINLESS)
        {
        $exception = exists $exceptions->{domainless}->{$2};
        # ignore "sometext": "some text here" for specific entries
        if (!$exception)
	  {
	  like($line, QR_VALID_LINE, "$file line $linenr: invalid entry syntax");
	  $number_of_tests_run ++;
	  next FILE;
	  }
        ($domain, $entry, $translation) = ('', $2, $3);
	}
      }
    else
      {
      ($domain, $entry, $translation) = ($1,$2,$3);
      }
    my @valid_domains = ( 'game', $modid, "block-$modid" );
    # is there some exception on a domain?
    # print STDERR "compatibility for $entry: $exceptions->{compatibility}->{$entry}\n";
    push @valid_domains, @{ $exceptions->{compatibility}->{$entry} } if exists $exceptions->{compatibility}->{$entry};

    my $qr_domain = '^(' . join('|', @valid_domains) . ')$';
    if (!$exception && $domain !~ /$qr_domain/)
      {
      like($domain, qr/$qr_domain/, "$file line $linenr: Entry must refer to game: or ${modid}: but is '$domain:$entry'");
      $number_of_tests_run ++;
      next LINE;
      }

    if (exists $entries{$entry} && !exists $exceptions->{compatibility}->{$entry})
      {
      my ($other_domain, $prev) = @{ $entries{$entry} };
      # #328: exception for incontainer liquid names
      # if these are defined in game and later in the modid, this is okay
      # Even in 1.17.11, these double translations are nec.
      # TODO: Should be fixed in 1.18
      if ($domain eq $modid && $other_domain eq 'game' && $entry =~ /incontainer/)
	{
	next LINE;
	}
      is ($prev, 0, "$file line $linenr: Entry $entry already defined with domain $other_domain in line $prev");
      $number_of_tests_run ++;
      next LINE;
      }

    if ($file =~ /\ben\.json/)
      {
      # For english, store the entry
      $english{"$domain:$entry"} = $translation;
      next LINE;
      }
    else
      {
      # For non-english, bail if the entry does not exist in the main english file
      my $exists = exists $english{"$domain:$entry"};
      if (!$exists)
        {
	# ignore exceptions
	my $exception = exists $exceptions->{overrides}->{"$domain:$entry"} || exists $exceptions->{overrides}->{$entry};
	if (!$exception)
	  {
	  is ($exists, 1, "$file line $linenr: Entry $entry is missing in English, left-over translation?");
          $number_of_tests_run ++;
	  }
	}
      else
        {
        # For non-english, bail if the entry is the same
	if ($english{"$domain:$entry"} eq $translation)
	  {
	  my $exception = exists $exceptions->{same}->{"$domain:$entry"};
	  if (!$exception)
	    {
	    isnt ($english{"$domain:$entry"}, $translation, "$file line $linenr: Entry $entry is untranslated");
            $number_of_tests_run ++;
	    }
	  }
	}

      }

    $entries{$entry} = [ $domain, $linenr ];
    }
  close ($FH);
  is ( $saw_open, 1, "File $file has no double entries, contains '{'");
  is ( $saw_close, 1, "File $file has no double entries, contains '}'");
  $number_of_tests_run += 2;

  # pass "1" as param to generate a list of all missing translations
  if ($generate_missing && $file !~ /\ben\.json/)
    {
    my @missing;
    # go through all english entries
    for my $entry (sort keys %english)
      {
      # "game:foo" => "game", "foo"
      my $bare_entry = $entry;
      $bare_entry =~ s/^([a-zA-Z0-9-]+)://; my $domain = $1;
      #print STDERR "Comparing '$entry' '$english{$entry}' vs. bare: '$bare_entry' '$entries{$bare_entry}'\n"; sleep(1);
      push @missing, "\t\"$entry\":\t\t\t\"$english{$entry}\""
	unless exists $entries{$bare_entry};
      }
    if (@missing > 0)
      {
      open (my $FHM, '>', "${file}.missing") or fail("${file}.missing - $!"), next;
      print $FHM "{\n";
      while (my $line = shift @missing)
	{
	# skip the "," on the last line
	print $FHM $line . (@missing == 0 ? '' : ',') . "\n";
	}
      print $FHM "}\n";
      close $FHM;
      }
    }
  }

# Test JSON files that mention handbook titles and texts
for my $json_file (sort @{ $info->{itemtypes} }, @{ $info->{blocktypes} } )
  {
  my $json = VSJSON::parse_json($json_file);

  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  # we expect only one item/block per JSON file
  is (ref($json), 'HASH', 'Expected an object, not an array');
  $number_of_tests_run ++;

  print "# $json_file\n" if DEBUG > 0;

  # build a list of valid states, so we can check textures referencing them
  my $states = [];
  if (exists $json->{variantgroups})
    {
    for my $state (@{$json->{variantgroups}})
      {
      my ($scode, $sstates) = VSJSON::generate_states_from_json($json_file, $state);

      push @$states, [ $scode, $sstates ];
      }
    }

  # for all properties in the object
  for my $key (sort keys %$json)
    {
    #        "attributesByType": {
    #    "powdered-boneash": {
    #            "handbook": { "extraSections": [ { "title": "handbook-item-usage", "text": "handbook-item-usage-powdered-boneash" } ] },
    #            "fertilizerProps": { "n": 0, "p": 42, "k": 0 }
    #    },
    if ($key eq 'attributesByType')
      {
      for my $entry (sort {
		# longest entries first
		length($b) <=> length($a) or
		$b cmp $a
		}
		keys %{ $json->{$key} })
        {
	my $att = $json->{$key}->{$entry};
	next unless ref($att);
	if (exists $att->{handbook} && $att->{handbook}->{extraSections})
	  {
	  my $sections = $att->{handbook}->{extraSections};
	  if (ref($sections) ne 'ARRAY')
	    {
	    is (ref($sections), 'ARRAY', "$json_file: ${entry}->handbook->extraSections is an ARRAY");
	    $number_of_tests_run ++;
	    }
	  for my $section (@$sections)
	    {
	    for my $ref ( sort keys %$section )
	      {
	      # skip entries like "text": "handbook-item-usage-powdered-{type}" for now
	      my $lang = $section->{$ref};
	      next if $lang =~ /[\{\*]/;
	      print STDERR "Checking $json_file $entry '$ref' $lang\n" if DEBUG;
	      $lang = 'game:' . $lang unless $lang =~ /^[a-z]+:/;
	      # skip this test if:
	      # * this entry refers to a different mod like em:
	      # * the original entry refers to "game:", then we assume it is in vanilla or a library
	      if ($lang !~ /^$dep/ && $section->{$ref} !~ /^game:/ && !exists $english{$lang})
	        {
		ok (exists $english{$lang}, "$json_file: attributesByType->${entry} $ref: '$lang' is in en.json");
		$number_of_tests_run ++;
		}
	      }
	    }
	  }
	}
      }
    } # end for all properties
  } # end for all items/blocks

done_testing( $number_of_tests_run );
