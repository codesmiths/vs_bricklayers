#!/usr/bin/perl

# Test that all script files compile and/or can be run.
# This test will also cover the testsuite itself and
# therefor should be run first.

use Test::More;
use File::Find;
use IPC::Run qw/run timeout/;
use warnings;
use strict;

my $number_of_tests_run = 0;

my @TEST_PATHS = ( 'build/', 'test/' );

# define the potential interpreters for scripting languages
my $SHEBANGS	= {
	sh => qr/\/bin\/(sh|bash)\b/,
	pl => qr/\/usr\/bin\/perl\b/,
	py => qr/\/usr\/bin\/env python\b/,
};
# define the way to check if a script compiles
# Format:
#   [ REGEXP, ARGUMENTS, EXPECTED_STDOUT, NOT_EXPECTED_OUT, EXPECTED_EXIT_CODE ]
#   REGEXP	- match the SHEBANG against this before trying
#   ##SHEBANG##	- the script mentioned in the shebang line
#   ##NAME##	- the script name
use constant COMPILES	=> {
	pl => [ [ qr/perl/, [ '##SHEBANG##', '-c', '##OPTIONS##', '##NAME##' ], qr/syntax OK/, qr/error/i, 1 ] ],
	t  => [
		[ qr/perl/, [ '##SHEBANG##', '-c', '##OPTIONS##', '##NAME##' ], qr/syntax OK/, qr/error/i, 1 ],
		[ qr/bash/, [ '##SHEBANG##', '-n', '##NAME##' ], qr/^\z/, qr/error/i, 1 ],
	]
};

my @EXE;

# define the subroutine to gather all executable files
sub is_exe {
  push @EXE, $File::Find::name
	if -f $File::Find::name && $File::Find::name =~ /\.(pl|py|t|sh)\z/;
};

# Gather all files
find({ wanted => \&is_exe, no_chdir => 1 }, @TEST_PATHS );

# test each file
for my $exe_file (@EXE)
  {
  ### Read the first line
  open (my $FH, '<', $exe_file) or fail("$exe_file - $!"), next;
  # read the first line
  my $firstline = <$FH>;
  close $FH;

  ### Extract the interpreter and options from the first line
  my ($script, $options);
  ($script, $options) = ($1, $2) if $firstline =~ /^#!([^\s\t]+)(.*?)[\r\n]+\z/;
  $number_of_tests_run ++;
  isnt ($script, undef, "$exe_file - first line looks valid");

  ### Find potential candidates to test the compilation with
  # for .t files (the default), we accept all of them
  my @candidates = (values %$SHEBANGS);
  # for other files, accept only a shebang line matching the specific extension
  if ($exe_file =~ /\.(pl|py|sh)\z/)
    {
    @candidates = ( $SHEBANGS->{$1} );
    }
  my $candidate_count = scalar @candidates;
  $number_of_tests_run ++;
  ok ($candidate_count > 0, "We know the type of $exe_file");

  next if $candidate_count < 1;

  ### Check the first line against all potential candidates
  my $match = undef;
  for my $regexp (@candidates)
    {
    $match = $regexp, last if $firstline =~ $regexp;
    }
  $number_of_tests_run ++;
  isnt ($match, undef, "$exe_file has a valid shebang line ($match)");

  ### If the shebang line is no valid, there is no point in trying to
  ### compile or run the script.
  next unless defined $match;

  ### Find the potential compiler / test executable
  @candidates = ();
  if ($exe_file =~ /\.(pl|t)\z/)
    {
    @candidates = @{ COMPILES->{$1} };
    }
  $candidate_count = scalar @candidates;
  next if $candidate_count < 1;

  for my $candidate (@candidates)
    {
    my ($regexp, $arguments, $expect_out, $not_expect, $expect_ec) = @$candidate;

    # use Data::Dumper; print Dumper(\@candidates);
    # print Dumper($regexp);
    # print Dumper($arguments);

    # does the script's shebang in question match the test candidate?
    if ($firstline =~ $regexp)
      {
      ### Prepare the command line arguments for the test compile
      my @final_args;
      # replace cmd line arguments with real values
      for my $carg (@$arguments)
        {
        # make a copy, so we can modify it
        my $arg = $carg;
        $arg =~ s/##SHEBANG##/$script/;
        $arg =~ s/##OPTIONS##/$options/;
        $arg =~ s/##NAME##/$exe_file/;

        $arg =~ s/^[\s\t]//;			# remove leading spaces
        push @final_args, $arg if $arg ne '';	# and skip empty parts
        }
      my $cmdline = join ("','", @final_args);

      #print STDERR "cmdline: '$cmdline'\n";

      ### Finally run the test compile, capturing the output
      my ($rc, $stdout, $stderr);
      eval {
        $rc = run ( \@final_args, \"", \$stdout, \$stderr, timeout(20) );
      };

      # all output from STDOUT and STDERR
      my $output = ($stdout // '') . ($stderr // '');

      $number_of_tests_run += 3;
      is ($rc, $expect_ec, "Could execute $cmdline");
      unlike ($output, $not_expect, "No errors compiling $exe_file");
      like ($output, $expect_out, "Got the expected output '$expect_out'");
      }
    } # end for the test candidate
  }

done_testing( $number_of_tests_run );
