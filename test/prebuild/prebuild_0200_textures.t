#!/bin/bash

# This script verifies that all texture files (pngs) in the
# asset tree have power-of-2 dimensions.
#
# A testfile - written as pure shell script.
#
# Output will be in the following format (X => number of tests)
#
# 1..X
# ok 1 - Name
# ok 2 - Other test
#
# Failing tests should create output like this:
# not ok 2 - Testname and reason for failure
#
# Count all files and print "1..X"
echo "1..`find assets/ -type f -name "*.png" | wc -l`"

# Print one ok/not ok line for each file
# find:  -print0 => use \x00 as line delimiter, so it works with file names with spaces
# xargs: -r 	 => do not run if empty list
#        -0 	 => accept \x00 as line delimiter
#        $fn 	 => filename
#        $mt 	 => mimetype
#        -IFOO	 => replace all occurances of FOO with the filename from find
# grep:  -o	 => output only the matching part
#	 -E	 => use extended Regexp syntax
# file:  -b	 => do not append filename to output
find assets/ -type f -name "*.png" -print0 | \
    xargs -r -0 -IFOO /bin/bash \
	-c 'fn="FOO" && mt=`file -b $fn | grep "\b(2|4|8|16|32|48|64|128|256|512|1024|2048) x (2|4|8|16|32|48|64|128|256|512|1024|2048)\b" -o -E`; if [[ $mt =~ " x " ]]; then echo "ok - $fn has power-of-2 dimensions ($mt)"; else echo "not ok - $fn is `file -b $fn`"; fi'
